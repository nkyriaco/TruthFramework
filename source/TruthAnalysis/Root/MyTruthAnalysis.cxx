#include <AsgMessaging/MessageCheck.h>
#include <TruthAnalysis/MyTruthAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
#include <xAODTruth/TruthEventContainer.h>
#include <algorithm>
#include <iostream>
#include <vector>
//Adding Matts include lines
/*
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include <truthAnalysis/truthAnalysis.h>
#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "PathResolver/PathResolver.h"
#include "xAODRootAccess/TEvent.h"
#include "AthContainers/ConstDataVector.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "TFile.h"
#include "TSystem.h"
*/

MyTruthAnalysis :: MyTruthAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

}

MyTruthAnalysis :: ~MyTruthAnalysis () {

}

bool MyTruthAnalysis::hasParent(const xAOD::TruthParticle *truth_part, int pdgID)
{
  for (unsigned int n=0; n<truth_part->nParents(); ++n)
      if (truth_part->parent(n)->pdgId()==pdgID) return true;
  return false;
}

float MyTruthAnalysis::DistanceFromPlane(float m1, float m2, float m3)
{
float distance = abs(m1 + k1*m2 + k2*m3)/sqrt(1+k1*k1+k2*k2);

return distance;
}

void MyTruthAnalysis::PTOrdering(float PairpT1, float PairpT2, float PairpT3,const xAOD::Jet *&jetH11, const xAOD::Jet *&jetH12, const xAOD::Jet *&jetH21, const xAOD::Jet *&jetH22, const xAOD::Jet *&jetH31, const xAOD::Jet *&jetH32, std::vector<const xAOD::Jet*> bjetspair1, std::vector<const xAOD::Jet*> bjetspair2, std::vector<const xAOD::Jet*> bjetspair3)
{

  //std::vector<const xAOD::Jet*> jetVector;

  if (PairpT1 > PairpT2 && PairpT2 > PairpT3) {
    jetH11 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[0] : bjetspair1[1];
    jetH12 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[1] : bjetspair1[0];
    jetH21 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[0] : bjetspair2[1];
    jetH22 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[1] : bjetspair2[0];
    jetH31 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[0] : bjetspair3[1];
    jetH32 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[1] : bjetspair3[0];
  }
  if (PairpT1 > PairpT3  && PairpT3 > PairpT2) {
    jetH11 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[0] : bjetspair1[1];
    jetH12 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[1] : bjetspair1[0];
    jetH21 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[0] : bjetspair3[1];
    jetH22 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[1] : bjetspair3[0];
    jetH31 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[0] : bjetspair2[1];
    jetH32 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[1] : bjetspair2[0];
  }
  if (PairpT2 > PairpT1 && PairpT1 > PairpT3) {
    jetH11 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[0] : bjetspair2[1];
    jetH12 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[1] : bjetspair2[0];
    jetH21 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[0] : bjetspair1[1];
    jetH22 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[1] : bjetspair1[0];
    jetH31 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[0] : bjetspair3[1];
    jetH32 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[1] : bjetspair3[0];
  }
  if (PairpT2 > PairpT3 && PairpT3 > PairpT1) {
    jetH11 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[0] : bjetspair2[1];
    jetH12 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[1] : bjetspair2[0];
    jetH21 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[0] : bjetspair3[1];
    jetH22 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[1] : bjetspair3[0];
    jetH31 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[0] : bjetspair1[1];
    jetH32 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[1] : bjetspair1[0];
  }
  if (PairpT3 > PairpT2 && PairpT2 > PairpT1) {
    jetH11 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[0] : bjetspair3[1];
    jetH12 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[1] : bjetspair3[0];
    jetH21 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[0] : bjetspair2[1];
    jetH22 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[1] : bjetspair2[0];
    jetH31 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[0] : bjetspair1[1];
    jetH32 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[1] : bjetspair1[0];
  }    
  if (PairpT3 > PairpT1 && PairpT1 > PairpT2) {
    jetH11 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[0] : bjetspair3[1];
    jetH12 = (bjetspair3[0]->pt() > bjetspair3[1]->pt()) ? bjetspair3[1] : bjetspair3[0];
    jetH21 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[0] : bjetspair1[1];
    jetH22 = (bjetspair1[0]->pt() > bjetspair1[1]->pt()) ? bjetspair1[1] : bjetspair1[0];
    jetH31 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[0] : bjetspair2[1];
    jetH32 = (bjetspair2[0]->pt() > bjetspair2[1]->pt()) ? bjetspair2[1] : bjetspair2[0];
  }
}

bool MyTruthAnalysis :: TruthPairing (const xAOD::JetContainer* jets, const xAOD::Jet *&jetH11, const xAOD::Jet *&jetH12, const xAOD::Jet *&jetH21, const xAOD::Jet *&jetH22, const xAOD::Jet *&jetH31, const xAOD::Jet *&jetH32 )
{

std::vector<const xAOD::TruthParticle*> bquarks;
std::vector<const xAOD::Jet*> bjets;
std::vector<const xAOD::TruthParticle*> parentboson;

const xAOD::Jet *jetPair1 = nullptr;
const xAOD::Jet *jetPair2 = nullptr;
const xAOD::Jet *jetPair3 = nullptr;


std::vector<const xAOD::TruthParticle*> bquarksPair1;
std::vector<const xAOD::Jet*> bjetsPair1;
std::vector<const xAOD::TruthParticle*> bquarksPair2;
std::vector<const xAOD::Jet*> bjetsPair2;
std::vector<const xAOD::TruthParticle*> bquarksPair3;
std::vector<const xAOD::Jet*> bjetsPair3;

const xAOD::TruthEventContainer *tec = 0;
ANA_CHECK(evtStore()->retrieve(tec,"TruthEvents"));
const xAOD::TruthParticleContainer *trueV = NULL;
ANA_CHECK(evtStore()->retrieve(trueV,"TruthBosonsWithDecayParticles"));
const xAOD::TruthParticleContainer *truthbpart = NULL;
ANA_CHECK(evtStore()->retrieve(truthbpart,"TruthBosonsWithDecayParticles"));

  for (const xAOD::TruthParticle* trueboson : *truthbpart ){
    // Used later for Truth Jet matching
    // 25 is the pdgID of the Higgs Bosons
    if (trueboson->pdgId() != 25) continue;

    for (size_t nchildren = 0; nchildren < trueboson->nChildren(); nchildren++){
      const xAOD::TruthParticle* child = trueboson->child(nchildren);
      const xAOD::Jet* closestjet = NULL;
      float tempDeltaR = 0.4;

      if (child->pdgId()!= -5 && child->pdgId() !=5) continue;
      if (child->status()!= 23) continue;
      bquarks.push_back(child);
      parentboson.push_back(trueboson);

    for (const xAOD::Jet* jet : *jets) {
      // First apply jet pT selection
      if (jet->pt()<20e3) continue;

      if (jet->p4().DeltaR(child->p4())>0.4) continue;

      else if (jet->p4().DeltaR(child->p4()) < tempDeltaR ) {
        tempDeltaR = jet->p4().DeltaR(child->p4());
        closestjet = jet;
      }


  }
  bjets.push_back(closestjet);
    }
  }


  int count = 0; //Starting count for our Higgs Pairs
  for (int i = 0; i < size(parentboson); i++) {
    for (int j = i+1; j < size(parentboson); j++) {
      if (parentboson[i]==parentboson[j]) {
        count++;
        if (count == 1 ){
          bquarksPair1.push_back(bquarks[i]);
          bquarksPair1.push_back(bquarks[j]);
          bjetsPair1.push_back(bjets[i]);
          bjetsPair1.push_back(bjets[j]);
        }
        if (count == 2 ){
          bquarksPair2.push_back(bquarks[i]);
          bquarksPair2.push_back(bquarks[j]);
          bjetsPair2.push_back(bjets[i]);
          bjetsPair2.push_back(bjets[j]);
        }
        if (count == 3 ){
          bquarksPair3.push_back(bquarks[i]);
          bquarksPair3.push_back(bquarks[j]);
          bjetsPair3.push_back(bjets[i]);
          bjetsPair3.push_back(bjets[j]);
        }
        // Here I create the kinematic variables
        // Think a little harder about Higgs Matching
      }
      if (count == 3) {
        // First check that we actually have 6 bjet pairs. If any of the jet pairs are NULL we return (exit the function)
        if (bjetsPair1[0] == NULL || bjetsPair1[1] == NULL || bjetsPair2[0] == NULL || bjetsPair2[1] == NULL || bjetsPair3[0] == NULL || bjetsPair3[1] == NULL) {
          return (false);
        }

        // Return if any of the bjet pairs are the same. 
        //It might pair the same jet to mutiple quarks if they end up in the same jet
        if (bjetsPair1[0] == bjetsPair1[1]) {
          return  (false);
        }
        if (bjetsPair1[0] == bjetsPair2[0]) {
          return  (false);
        }
        if (bjetsPair1[0] == bjetsPair2[1]) {
          return  (false);
        }
        if (bjetsPair1[0] == bjetsPair3[0]) {
          return  (false);
        }
        if (bjetsPair1[0] == bjetsPair3[1]) {
          return  (false);
        }
        if (bjetsPair1[1] == bjetsPair2[0]) {
          return  (false);
        }
        if (bjetsPair1[1] == bjetsPair2[1]) {
          return  (false);
        }     
        if (bjetsPair1[1] == bjetsPair3[0]) {
          return  (false);
        } 
        if (bjetsPair1[1] == bjetsPair3[1]) {
          return  (false);
        }    
        if (bjetsPair2[0] == bjetsPair2[1]) {
          return  (false);
        }     
        if (bjetsPair2[0] == bjetsPair3[0]) {
          return  (false);
        } 
        if (bjetsPair2[0] == bjetsPair3[1]) {
          return  (false);
        }    

        if (bjetsPair2[1] == bjetsPair3[0]) {
          return  (false);
        } 
        if (bjetsPair2[1] == bjetsPair3[1]) {
          return  (false);
        }  
        if (bjetsPair3[0] == bjetsPair3[1]) {
          return  (false);
        } 

        // Now we match jets to those with the highest pT
        // We define the pT as the total pT of the paired jet. Not the sum of the two pairs pT's. 
        float PairpT1 = (bjetsPair1[0]->p4() + bjetsPair1[1]->p4()).Pt();
        float PairpT2 = (bjetsPair2[0]->p4() + bjetsPair2[1]->p4()).Pt();
        float PairpT3 = (bjetsPair3[0]->p4() + bjetsPair3[1]->p4()).Pt();

        if (DoPTOrdering==true) {
          
        PTOrdering(PairpT1, PairpT2, PairpT3, jetH11, jetH12, jetH21, jetH22, jetH31, jetH32, bjetsPair1, bjetsPair2, bjetsPair3);

        }

      } // End of count==3 check
    }

  } // End of looping through both the two b-jets for a Higgs Pair
  return true;
}

StatusCode MyTruthAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO ("in initialize");

  ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]
  ANA_CHECK (book (TH1F ("fullweight", "fullweight",10,1,11))); // MC Channel Weights

  ANA_CHECK (book (TTree ("analysis", "Truth Analysis ntuple")));
  TTree* mytree = tree ("analysis");
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);
  jetEtaH11 = new std::vector<float>();
  jetEtaH12 = new std::vector<float>();
  jetEtaH21 = new std::vector<float>();
  jetEtaH22 = new std::vector<float>();
  jetEtaH31 = new std::vector<float>();
  jetEtaH32 = new std::vector<float>();
  mytree->Branch ("JetEtaH11", &jetEtaH11);
  mytree->Branch ("JetEtaH12", &jetEtaH12);
  mytree->Branch ("JetEtaH21", &jetEtaH21);
  mytree->Branch ("JetEtaH22", &jetEtaH22);
  mytree->Branch ("JetEtaH31", &jetEtaH31);
  mytree->Branch ("JetEtaH32", &jetEtaH32);
  jetPhiH11 = new std::vector<float>();
  jetPhiH12 = new std::vector<float>();
  jetPhiH21 = new std::vector<float>();
  jetPhiH22 = new std::vector<float>();
  jetPhiH31 = new std::vector<float>();
  jetPhiH32 = new std::vector<float>();
  mytree->Branch ("JetPhiH11", &jetPhiH11);
  mytree->Branch ("JetPhiH12", &jetPhiH12);
  mytree->Branch ("JetPhiH21", &jetPhiH21);
  mytree->Branch ("JetPhiH22", &jetPhiH22);
  mytree->Branch ("JetPhiH31", &jetPhiH31);
  mytree->Branch ("JetPhiH32", &jetPhiH32);
  jetPtH11 = new std::vector<float>();
  jetPtH12 = new std::vector<float>();
  jetPtH21 = new std::vector<float>();
  jetPtH22 = new std::vector<float>();
  jetPtH31 = new std::vector<float>();
  jetPtH31 = new std::vector<float>();
  mytree->Branch ("JetPtH11", &jetPtH11);
  mytree->Branch ("JetPtH12", &jetPtH12);
  mytree->Branch ("JetPtH21", &jetPtH21);
  mytree->Branch ("JetPtH22", &jetPtH22);
  mytree->Branch ("JetPtH31", &jetPtH31);
  mytree->Branch ("JetPtH32", &jetPtH32);
  //jetE = new std::vector<float>();
  //mytree->Branch ("JetE", &jetE);
  jetRapidityH11 = new std::vector<float>();
  jetRapidityH12 = new std::vector<float>();
  jetRapidityH21 = new std::vector<float>();
  jetRapidityH22 = new std::vector<float>();
  jetRapidityH31 = new std::vector<float>();
  jetRapidityH32 = new std::vector<float>();
  mytree->Branch ("jetRapidityH11", &jetRapidityH11);
  mytree->Branch ("jetRapidityH12", &jetRapidityH12);
  mytree->Branch ("jetRapidityH21", &jetRapidityH21);
  mytree->Branch ("jetRapidityH22", &jetRapidityH22);
  mytree->Branch ("jetRapidityH31", &jetRapidityH31);
  mytree->Branch ("jetRapidityH32", &jetRapidityH32);
  jetmassH11 = new std::vector<float>();
  jetmassH12 = new std::vector<float>();
  jetmassH21 = new std::vector<float>();
  jetmassH22 = new std::vector<float>();
  jetmassH31 = new std::vector<float>();
  jetmassH32 = new std::vector<float>();
  mytree->Branch("jetmassH11",&jetmassH11);
  mytree->Branch("jetmassH12",&jetmassH12);
  mytree->Branch("jetmassH21",&jetmassH21);
  mytree->Branch("jetmassH22",&jetmassH22);
  mytree->Branch("jetmassH31",&jetmassH31);
  mytree->Branch("jetmassH32",&jetmassH32);

  // Higgs Variables
  EtaH1 = new std::vector<float>();
  EtaH2 = new std::vector<float>();
  EtaH3 = new std::vector<float>();
  dEtaH1 = new std::vector<float>();
  dEtaH2 = new std::vector<float>();
  dEtaH3 = new std::vector<float>();
  dEtaH1H2 = new std::vector<float>();
  dEtaH2H3 = new std::vector<float>();
  dEtaH1H3 = new std::vector<float>();
  mytree->Branch ("EtaH1", &EtaH1);
  mytree->Branch ("EtaH2", &EtaH2);
  mytree->Branch ("EtaH3", &EtaH3);
  mytree->Branch ("dEtaH1", &dEtaH1);
  mytree->Branch ("dEtaH2", &dEtaH2);
  mytree->Branch ("dEtaH3", &dEtaH3);
  mytree->Branch ("dEtaH1H2", &dEtaH1H2);
  mytree->Branch ("dEtaH1H3", &dEtaH1H3);
  mytree->Branch ("dEtaH2H3", &dEtaH2H3);

  PhiH1 = new std::vector<float>();
  PhiH2 = new std::vector<float>();
  PhiH3 = new std::vector<float>();
  dPhiH1 = new std::vector<float>();
  dPhiH2 = new std::vector<float>();
  dPhiH3 = new std::vector<float>();
  dPhiH1H2 = new std::vector<float>();
  dPhiH1H3 = new std::vector<float>();
  dPhiH2H3 = new std::vector<float>();
  mytree->Branch ("PhiH1", &PhiH1);
  mytree->Branch ("PhiH2", &PhiH2);
  mytree->Branch ("PhiH3", &PhiH3);
  mytree->Branch ("dPhiH1", &dPhiH1);
  mytree->Branch ("dPhiH2", &dPhiH2);
  mytree->Branch ("dPhiH3", &dPhiH3);
  mytree->Branch ("dPhiH1H2", &dPhiH1H2);
  mytree->Branch ("dPhiH1H3", &dPhiH1H3);
  mytree->Branch ("dPhiH2H3", &dPhiH2H3);

  mH1 = new std::vector<float>();
  mH2 = new std::vector<float>();
  mH3 = new std::vector<float>();
  mytree->Branch ("mH1", &mH1);
  mytree->Branch ("mH2", &mH2);
  mytree->Branch ("mH3", &mH3);

  pTH1 = new std::vector<float>();
  pTH2 = new std::vector<float>();
  pTH3 = new std::vector<float>();
  pTHHH = new std::vector<float>();
  dpTH1 = new std::vector<float>();
  dpTH2 = new std::vector<float>();
  dpTH3 = new std::vector<float>();
  dpTH1H2 = new std::vector<float>();
  dpTH1H3 = new std::vector<float>();
  dpTH2H3 = new std::vector<float>();
  mytree->Branch ("pTH1", &pTH1);
  mytree->Branch ("pTH2", &pTH2);
  mytree->Branch ("pTH3", &pTH3);
  mytree->Branch ("pTHHH", &pTHHH);
  mytree->Branch ("dpTH1", &dpTH1);
  mytree->Branch ("dpTH2", &dpTH2);
  mytree->Branch ("dpTH3", &dpTH3);
  mytree->Branch ("dpTH1H2", &dpTH1H2);
  mytree->Branch ("dpTH1H3", &dpTH1H3);
  mytree->Branch ("dpTH2H3", &dpTH2H3);

  RapidityH1 = new std::vector<float>();
  RapidityH2 = new std::vector<float>();
  RapidityH3 = new std::vector<float>();
  dRapidityH1 = new std::vector<float>();
  dRapidityH2 = new std::vector<float>();
  dRapidityH3 = new std::vector<float>();
  dRapidityH1H2 = new std::vector<float>();
  dRapidityH1H3 = new std::vector<float>();
  dRapidityH2H3 = new std::vector<float>();
  mytree->Branch ("RapidityH1", &RapidityH1);
  mytree->Branch ("RapidityH2", &RapidityH2);
  mytree->Branch ("RapidityH3", &RapidityH3);
  mytree->Branch ("dRapidityH1", &dRapidityH1);
  mytree->Branch ("dRapidityH2", &dRapidityH2);
  mytree->Branch ("dRapidityH3", &dRapidityH3);
  mytree->Branch ("dRapidityH1H2", &dRapidityH1H2);
  mytree->Branch ("dRapidityH1H3", &dRapidityH1H3);
  mytree->Branch ("dRapidityH2H3", &dRapidityH2H3);

  mH1H2 = new std::vector<float>();
  mH2H3 = new std::vector<float>();
  mH1H3 = new std::vector<float>();
  mHHH = new std::vector<float>();
  mytree->Branch ("mH1H2", &mH1H2);
  mytree->Branch ("mH2H3", &mH2H3);
  mytree->Branch ("mH1H3", &mH1H3);
  mytree->Branch ("mHHH", &mHHH);

  nJets = new std::vector<int>();
  mytree->Branch ("nJets", &nJets);
  // These variables are probably not needed. Hold for now as a template. 
  /*
  jeteff = new std::vector<float>();
  mytree->Branch("jeteff",&jeteff);
  jet60 = new std::vector<char>();
  mytree->Branch("jet60",&jet60);
  jet70 = new std::vector<char>();
  mytree->Branch("jet70",&jet70);
  jet77 = new std::vector<char>();
  mytree->Branch("jet77",&jet77);
  jet85 = new std::vector<char>();
  mytree->Branch("jet85",&jet85);
  jetbcount = new std::vector<int>();
  mytree->Branch("jetbcount",&jetbcount);
  jetparton = new std::vector<int>();
  mytree->Branch("jetparton",&jetparton);
  jetccount = new std::vector<int>();
  mytree->Branch("jetccount",&jetccount);
  p23status = new std::vector<int>();
  mytree->Branch("p23status",&p23status);
  bqpt = new std::vector<float>();
  mytree->Branch("bqpt",&bqpt);
  bqeta = new std::vector<float>();
  mytree->Branch("bqeta",&bqeta);
  bqphi = new std::vector<float>();
  mytree->Branch("bqphi",&bqphi);
  bqcharge = new std::vector<float>();
  mytree->Branch("bqcharge",&bqcharge);
  bqstatus = new std::vector<int>();
  mytree->Branch("bqstatus",&bqstatus);
  */
  mytree->Branch("weight",&weight);
  mytree->Branch("identification",&identification);
  mytree->Branch("phaseM",&phaseM);
  mytree->Branch("phasePt",&phasePt);


  return StatusCode::SUCCESS;
}



StatusCode MyTruthAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ANA_MSG_INFO ("in execute");

  // Declaring a global variable that I need 

  // Read/fill the EventInfo variables:
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  weight = (eventInfo->mcEventWeights().size() > 0) ? (eventInfo->mcEventWeights())[0] : 1.;
  
  // print out run and event number from retrieved object
  ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  // Clearing our vectors that we push back
  jetEtaH11->clear();
  jetEtaH12->clear();
  jetEtaH21->clear();
  jetEtaH22->clear();
  jetEtaH31->clear();
  jetEtaH32->clear();

  jetPhiH11->clear();
  jetPhiH12->clear();
  jetPhiH21->clear();
  jetPhiH22->clear();
  jetPhiH31->clear();
  jetPhiH32->clear();

  jetPtH11->clear();
  jetPtH12->clear();
  jetPtH21->clear();
  jetPtH22->clear();
  jetPtH31->clear();
  jetPtH32->clear();

  jetRapidityH11->clear();
  jetRapidityH12->clear();
  jetRapidityH21->clear();
  jetRapidityH22->clear();
  jetRapidityH31->clear();
  jetRapidityH32->clear();

  jetmassH11->clear();
  jetmassH12->clear();
  jetmassH21->clear();
  jetmassH22->clear();
  jetmassH31->clear();
  jetmassH32->clear();

  EtaH1->clear();
  EtaH2->clear();
  EtaH3->clear();
  dEtaH1->clear();
  dEtaH1->clear();
  dEtaH1->clear();
  dEtaH1H2->clear();
  dEtaH1H3->clear();
  dEtaH2H3->clear();

  PhiH1->clear();
  PhiH2->clear();
  PhiH3->clear();
  dPhiH1->clear();
  dPhiH2->clear();
  dPhiH3->clear();
  dPhiH1H2->clear();
  dPhiH1H3->clear();
  dPhiH2H3->clear();

  mH1->clear();
  mH2->clear();
  mH3->clear();

  pTH1->clear();
  pTH2->clear();
  pTH3->clear();
  pTHHH->clear();
  dpTH1->clear();
  dpTH2->clear();
  dpTH3->clear();
  dpTH1H2->clear();
  dpTH1H3->clear();
  dpTH2H3->clear();

  RapidityH1->clear();
  RapidityH2->clear();
  RapidityH3->clear();
  dRapidityH1->clear();
  dRapidityH2->clear();
  dRapidityH3->clear();
  dRapidityH1H2->clear();
  dRapidityH1H3->clear();
  dRapidityH2H3->clear();

  mH1H2->clear();
  mH2H3->clear();
  mH1H3->clear();
  mHHH->clear();

  nJets->clear();

  // loop over the (truth) jets in the container
  // get jet container of interest

  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets, "AntiKt4TruthDressedWZJets"));
  ANA_MSG_INFO ("execute(): number of jets = " << jets->size());

  // Counting tools and truth higgs b-jets containers defined here
  int countb = 0;
  int countj1 = 0;
  int countj2 = 0;

  const xAOD::Jet *jetH11 = nullptr;
  const xAOD::Jet *jetH12 = nullptr;
  const xAOD::Jet *jetH21 = nullptr;
  const xAOD::Jet *jetH22 = nullptr;
  const xAOD::Jet *jetH31 = nullptr;
  const xAOD::Jet *jetH32 = nullptr; 

  // If event passes selection then add to fullweights
  // Double check this is where we want it filled to.....
  hist("fullweight")->Fill(1.1,1.0);
  hist("fullweight")->Fill(2.1,weight);
  
if (DoTruthPairing==true) {

bool DidGoodJob = TruthPairing(jets, jetH11, jetH12, jetH21, jetH22, jetH31, jetH32 );
  
  if (!DidGoodJob)
  {
    //Exit if we did not do a good job
    return StatusCode::SUCCESS;
  }

}

if (DoTruthPairing==false) {
  // Need to implement my new method
  // We need to choose some basic higgs pair ordering.

  // We begin by looping through all the b-jets in our selection. 

  int jet_counter = 0;

  // Before beginning this pairing algorithm I make sure I have 6 jets
  if (jets->size() < 6) return StatusCode::SUCCESS;
  std::vector<float> JetPT; // List of pT's across b-jets
  std::vector<int> JetIndex; // Stores index of 6 jets with highest pT
  std::vector<const xAOD::Jet*> Jets; // We need to make a vector of jets within the container.
  std::vector<const xAOD::Jet*> HiggsJets; // Here is where we have the pT sorted Higgs Jets
  // Now we take each jet in the container, and store them in a vector of jets
  // Just take the 6 with the highest pT
  for (const xAOD::Jet* jet : *jets) {
      // First apply jet pT selection
      if (jet->pt()<20e3) continue;
      JetPT.push_back(jet->pt());
      Jets.push_back(jet);
  }

  // We need to have more than 6 jets pass our selection requirements
  // Maybe not necessary overall (but for now to keep comparison with truth matching we include this line)
  if (Jets.size() < 6) return StatusCode::SUCCESS;

  // These lines commented out were used to sort the jets by pT. Apparently this is already done...
  /*
  std::vector<float> y(JetPT.size());
  std::size_t n(0);
  std::generate(std::begin(y), std::end(y), [&]{ return n++; });

  std::sort(  std::begin(y), 
              std::end(y),
              [&](int i1, int i2) { return JetPT[i1] > JetPT[i2]; } );

  for (auto v : y) {
    std::cout << v << ' ';
    std::cout << JetPT[v] << std::endl;
  }
  */
  
  std::cout << "This is a test " << std::endl;
  std::cout << Jets.size() << std::endl;
  for (int i = 0; i < 6; i++) {
    HiggsJets.push_back(Jets[i]);
  }

  // Now we can loop through each of the six b-jets and assign our pairing algorithm based on the masses.

  int num_columns = 15;
  int num_rows = 6;
  std::vector<std::vector<const xAOD::Jet*>> JetMatrix; // Here is where we have different combinations of Higgs Jets
  for (int i = 0 ; i < num_columns; i++) {
    JetMatrix.push_back(HiggsJets);
  }

  std::cout << JetMatrix.size() << std::endl;
  std::cout << JetMatrix[0].size() << std::endl;


  float tmpmH1 = 0;
  float tmpmH2 = 0;
  float tmpmH3 = 0;
  float distance = 1e9; // Have a very large placeholder value for this distance
  std::vector<const xAOD::Jet*> tempJets; //Temporary vector jets for each Higgs Pair Combination
  int index_counter;
  // Here we perform our distance calculation for each b-jet pairing
  for (int i = 0; i<JetMatrix.size(); i++) {
    tempJets = JetMatrix[i];
    tmpmH1 = (tempJets[0]->p4() + tempJets[1]->p4()).M();
    tmpmH2 = (tempJets[2]->p4() + tempJets[3]->p4()).M();
    tmpmH3 = (tempJets[4]->p4() + tempJets[5]->p4()).M();
    if (DistanceFromPlane(tmpmH1,tmpmH2,tmpmH3) < distance ) {
      distance = DistanceFromPlane(tmpmH1,tmpmH2,tmpmH3);
      index_counter = i;
    }
  }
  // Now that we have found which higgs pair gives the smallest mass difference we can then reconstruct the kinematics!
  jetH11 = JetMatrix[index_counter][0];
  jetH12 = JetMatrix[index_counter][1];
  jetH21 = JetMatrix[index_counter][2];
  jetH22 = JetMatrix[index_counter][3];
  jetH31 = JetMatrix[index_counter][4];
  jetH32 = JetMatrix[index_counter][5];

  /*
  // Now that we have the jets sorted by their pT. We need to make a vector of jets within the container.
  std::vector<const xAOD::Jet*> HiggsJets;
  int counter = 0;
  for (const xAOD::Jet* jet : *jets) {
    if (jet->pt()<20e3) continue;
  
    HiggsJets.push_back(jet)
    counter++;
  }
  */

          
  // Rather I want to go through all 15 combinations and match the pairing by mimizing
  // Difference from mass plane. Ask Matt how this conflicts with my pairing at truth-level....
  //tmpmH1 = (bjetsPair1[0]->p4() + bjetsPair1[1]->p4()).M();
  //tmpmH2 = (bjetsPair2[0]->p4() + bjetsPair2[1]->p4()).M();
  //tmpmH3 = (bjetsPair3[0]->p4() + bjetsPair3[1]->p4()).M();

  //float distance = DistanceFromPlane(tmpmH1,tmpmH2,tmpmH3);
}


  //Defining TLorentz vectors to do actual kinematics
  TLorentzVector TL_H11 = jetH11->p4();
  TLorentzVector TL_H12 = jetH12->p4();
  TLorentzVector TL_H21 = jetH21->p4();
  TLorentzVector TL_H22 = jetH22->p4();
  TLorentzVector TL_H31 = jetH31->p4();
  TLorentzVector TL_H32 = jetH32->p4();

  TL_H11.SetPtEtaPhiM(jetH11->pt(),jetH11->eta(),jetH11->phi(),jetH11->m());
  TL_H12.SetPtEtaPhiM(jetH12->pt(),jetH12->eta(),jetH12->phi(),jetH12->m());
  TL_H21.SetPtEtaPhiM(jetH21->pt(),jetH21->eta(),jetH21->phi(),jetH21->m());
  TL_H22.SetPtEtaPhiM(jetH22->pt(),jetH22->eta(),jetH22->phi(),jetH22->m());
  TL_H31.SetPtEtaPhiM(jetH31->pt(),jetH31->eta(),jetH31->phi(),jetH31->m());
  TL_H32.SetPtEtaPhiM(jetH32->pt(),jetH32->eta(),jetH32->phi(),jetH32->m());

  TLorentzVector TL_H1 = TL_H11 + TL_H12;
  TLorentzVector TL_H2 = TL_H21 + TL_H22;
  TLorentzVector TL_H3 = TL_H31 + TL_H32;


  if (jetH11->pt()<20e3 || abs(jetH11->eta())>2.5) return StatusCode::SUCCESS;
  if (jetH12->pt()<20e3 || abs(jetH12->eta())>2.5) return StatusCode::SUCCESS;
  if (jetH21->pt()<20e3 || abs(jetH21->eta())>2.5) return StatusCode::SUCCESS;
  if (jetH22->pt()<20e3 || abs(jetH22->eta())>2.5) return StatusCode::SUCCESS;
  if (jetH31->pt()<20e3 || abs(jetH31->eta())>2.5) return StatusCode::SUCCESS;
  if (jetH32->pt()<20e3 || abs(jetH32->eta())>2.5) return StatusCode::SUCCESS;


  // Now that we have paired all the b-jets we need to fill them. 
  jetEtaH11->push_back(jetH11->eta());
  jetEtaH12->push_back(jetH12->eta());
  jetEtaH21->push_back(jetH21->eta());
  jetEtaH22->push_back(jetH22->eta());
  jetEtaH31->push_back(jetH31->eta());
  jetEtaH32->push_back(jetH32->eta());

  jetPhiH11->push_back(jetH11->phi());
  jetPhiH12->push_back(jetH21->phi());
  jetPhiH21->push_back(jetH21->phi());
  jetPhiH22->push_back(jetH22->phi());
  jetPhiH31->push_back(jetH31->phi());
  jetPhiH32->push_back(jetH32->phi());

  jetPtH11->push_back(jetH11->pt());
  jetPtH12->push_back(jetH12->pt());
  jetPtH21->push_back(jetH21->pt());
  jetPtH22->push_back(jetH22->pt());
  jetPtH31->push_back(jetH31->pt());
  jetPtH32->push_back(jetH32->pt());

  jetRapidityH11->push_back(jetH11->rapidity());
  jetRapidityH12->push_back(jetH12->rapidity());
  jetRapidityH21->push_back(jetH21->rapidity());
  jetRapidityH22->push_back(jetH22->rapidity());
  jetRapidityH31->push_back(jetH31->rapidity());
  jetRapidityH32->push_back(jetH32->rapidity());

  jetmassH11->push_back(jetH11->m());
  jetmassH12->push_back(jetH12->m());
  jetmassH21->push_back(jetH21->m());
  jetmassH22->push_back(jetH22->m());
  jetmassH31->push_back(jetH31->m());
  jetmassH32->push_back(jetH32->m());

  // Fill the Higgs variables
  EtaH1->push_back(TL_H1.Eta());
  EtaH2->push_back(TL_H2.Eta());
  EtaH3->push_back(TL_H3.Eta());
  dEtaH1->push_back(fabs(jetH11->eta()-jetH12->eta()));
  dEtaH2->push_back(fabs(jetH21->eta()-jetH22->eta()));
  dEtaH3->push_back(fabs(jetH31->eta()-jetH32->eta()));
  dEtaH1H2->push_back(fabs(TL_H1.Eta()-TL_H2.Eta()));
  dEtaH1H3->push_back(fabs(TL_H1.Eta()-TL_H3.Eta()));
  dEtaH2H3->push_back(fabs(TL_H2.Eta()-TL_H3.Eta()));

  PhiH1->push_back(TL_H1.Phi());
  PhiH2->push_back(TL_H2.Phi());
  PhiH3->push_back(TL_H3.Phi());
  dPhiH1->push_back(fabs(jetH11->phi()-jetH12->phi()));
  dPhiH2->push_back(fabs(jetH21->phi()-jetH22->phi()));
  dPhiH3->push_back(fabs(jetH31->phi()-jetH32->phi()));
  dPhiH1H2->push_back(fabs(TL_H1.Phi()-TL_H2.Phi()));
  dPhiH1H3->push_back(fabs(TL_H1.Phi()-TL_H3.Phi()));
  dPhiH2H3->push_back(fabs(TL_H2.Phi()-TL_H3.Phi()));

  mH1->push_back(TL_H1.M());
  mH2->push_back(TL_H2.M());
  mH3->push_back(TL_H3.M());  

  RapidityH1->push_back(TL_H1.Rapidity());
  RapidityH2->push_back(TL_H2.Rapidity());
  RapidityH3->push_back(TL_H3.Rapidity());
  dRapidityH1->push_back(fabs(jetH11->rapidity()-jetH12->rapidity()));
  dRapidityH2->push_back(fabs(jetH21->rapidity()-jetH22->rapidity()));
  dRapidityH3->push_back(fabs(jetH31->rapidity()-jetH32->rapidity()));
  dRapidityH1H2->push_back(fabs(TL_H1.Rapidity()-TL_H2.Rapidity()));
  dRapidityH1H3->push_back(fabs(TL_H1.Rapidity()-TL_H3.Rapidity()));
  dRapidityH2H3->push_back(fabs(TL_H2.Rapidity()-TL_H3.Rapidity()));

  pTH1->push_back(TL_H1.Pt());
  pTH2->push_back(TL_H2.Pt());
  pTH3->push_back(TL_H3.Pt());
  pTHHH->push_back((TL_H1+TL_H2+TL_H3).Pt());
  dpTH1->push_back(fabs(jetH11->pt()-jetH12->pt()));
  dpTH2->push_back(fabs(jetH21->pt()-jetH22->pt()));
  dpTH3->push_back(fabs(jetH31->pt()-jetH32->pt()));
  dpTH1H2->push_back(fabs(TL_H1.Pt()-TL_H2.Pt()));
  dpTH1H3->push_back(fabs(TL_H1.Pt()-TL_H3.Pt()));
  dpTH2H3->push_back(fabs(TL_H2.Pt()-TL_H3.Pt()));

  mH1H2->push_back((TL_H1+TL_H2).M());
  mH2H3->push_back((TL_H2+TL_H3).M());
  mH1H3->push_back((TL_H1+TL_H3).M());
  mHHH->push_back((TL_H1+TL_H2+TL_H3).M());

  nJets->push_back(jets->size());

  // Fill the event into the tree:
  tree ("analysis")->Fill();

  return StatusCode::SUCCESS;
}



StatusCode MyTruthAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}