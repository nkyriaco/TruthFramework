#ifndef TruthAnalysis_MyTruthAnalysis_H
#define TruthAnalysis_MyTruthAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
#include <TH1.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <TTree.h>

float k1 = 125/120;
float k2 = 1.08;
bool DoPTOrdering = false;
bool DoTruthPairing = false;


class MyTruthAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MyTruthAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  ~MyTruthAnalysis ();
  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

private:

  // My pretty function (that I don't use)
  bool hasParent(const xAOD::TruthParticle *truth_part, int pdgID);
  float DistanceFromPlane(float m1, float m2, float m3);
  void PTOrdering(float PairpT1, float PairpT2, float PairpT3, const xAOD::Jet *&jetH11, const xAOD::Jet *&jetH12, const xAOD::Jet *&jetH21, const xAOD::Jet *&jetH22, const xAOD::Jet *&jetH31, const xAOD::Jet *&jetH32, std::vector<const xAOD::Jet*> bjetspair1, std::vector<const xAOD::Jet*> bjetspair2, std::vector<const xAOD::Jet*> bjetspair3);
  bool TruthPairing(const xAOD::JetContainer* jets, const xAOD::Jet *&jetH11, const xAOD::Jet *&jetH12, const xAOD::Jet *&jetH21, const xAOD::Jet *&jetH22, const xAOD::Jet *&jetH31, const xAOD::Jet *&jetH32 );
  // Configuration, and any other types of variables go here.

  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  float weight;
  int identification;
  float phaseM;
  float phasePt;


  /// Jet 4-momentum variables
  std::vector<float> *jeteff = nullptr;
  std::vector<char> *jet60 = nullptr;
  std::vector<char> *jet70 = nullptr;
  std::vector<char> *jet77 = nullptr;
  std::vector<char> *jet85 = nullptr;
  std::vector<int> *jetbcount = nullptr;
  std::vector<int> *p23status = nullptr;
  std::vector<int> *jetparton = nullptr;
  std::vector<int> *jetccount = nullptr;
  std::vector<float> *bqpt = nullptr;
  std::vector<float> *bqeta = nullptr;
  std::vector<float> *bqphi = nullptr;
  std::vector<float> *bqcharge = nullptr;
  std::vector<int> *bqstatus = nullptr;

  std::vector<float> *jetEtaH11 = nullptr;
  std::vector<float> *jetPhiH11 = nullptr;
  std::vector<float> *jetPtH11 = nullptr;
  std::vector<float> *jetRapidityH11 = nullptr;
  std::vector<float> *jetmassH11 = nullptr;

  std::vector<float> *jetEtaH12 = nullptr;
  std::vector<float> *jetPhiH12 = nullptr;
  std::vector<float> *jetPtH12 = nullptr;
  std::vector<float> *jetRapidityH12 = nullptr;
  std::vector<float> *jetmassH12 = nullptr;

  std::vector<float> *jetEtaH21 = nullptr;
  std::vector<float> *jetPhiH21 = nullptr;
  std::vector<float> *jetPtH21 = nullptr;
  std::vector<float> *jetRapidityH21 = nullptr;
  std::vector<float> *jetmassH21 = nullptr;

  std::vector<float> *jetEtaH22 = nullptr;
  std::vector<float> *jetPhiH22 = nullptr;
  std::vector<float> *jetPtH22 = nullptr;
  std::vector<float> *jetRapidityH22 = nullptr;
  std::vector<float> *jetmassH22 = nullptr;

  std::vector<float> *jetEtaH31 = nullptr;
  std::vector<float> *jetPhiH31 = nullptr;
  std::vector<float> *jetPtH31 = nullptr;
  std::vector<float> *jetRapidityH31 = nullptr;
  std::vector<float> *jetmassH31 = nullptr;

  std::vector<float> *jetEtaH32 = nullptr;
  std::vector<float> *jetPhiH32 = nullptr;
  std::vector<float> *jetPtH32 = nullptr;
  std::vector<float> *jetRapidityH32 = nullptr;
  std::vector<float> *jetmassH32 = nullptr;

  // Higgs Variables
  std::vector<float> *EtaH1 = nullptr;
  std::vector<float> *EtaH2 = nullptr;
  std::vector<float> *EtaH3 = nullptr;
  std::vector<float> *dEtaH1 = nullptr;
  std::vector<float> *dEtaH2 = nullptr;
  std::vector<float> *dEtaH3 = nullptr;
  std::vector<float> *dEtaH1H2 = nullptr;
  std::vector<float> *dEtaH1H3 = nullptr;
  std::vector<float> *dEtaH2H3 = nullptr;

  std::vector<float> *PhiH1 = nullptr;
  std::vector<float> *PhiH2 = nullptr;
  std::vector<float> *PhiH3 = nullptr;
  std::vector<float> *dPhiH1 = nullptr;
  std::vector<float> *dPhiH2 = nullptr;
  std::vector<float> *dPhiH3 = nullptr;
  std::vector<float> *dPhiH1H2 = nullptr;
  std::vector<float> *dPhiH1H3 = nullptr;
  std::vector<float> *dPhiH2H3 = nullptr;

  std::vector<float> *mH1 = nullptr;
  std::vector<float> *mH2 = nullptr;
  std::vector<float> *mH3 = nullptr;

  std::vector<float> *pTH1 = nullptr;
  std::vector<float> *pTH2 = nullptr;
  std::vector<float> *pTH3 = nullptr;
  std::vector<float> *pTHHH = nullptr;
  std::vector<float> *dpTH1 = nullptr;
  std::vector<float> *dpTH2 = nullptr;
  std::vector<float> *dpTH3 = nullptr;
  std::vector<float> *dpTH1H2 = nullptr;
  std::vector<float> *dpTH1H3 = nullptr;
  std::vector<float> *dpTH2H3 = nullptr;

  std::vector<float> *RapidityH1 = nullptr;
  std::vector<float> *RapidityH2 = nullptr;
  std::vector<float> *RapidityH3 = nullptr;
  std::vector<float> *dRapidityH1 = nullptr;
  std::vector<float> *dRapidityH2 = nullptr;
  std::vector<float> *dRapidityH3 = nullptr;
  std::vector<float> *dRapidityH1H2 = nullptr;
  std::vector<float> *dRapidityH1H3 = nullptr;
  std::vector<float> *dRapidityH2H3 = nullptr;

  std::vector<float> *mH1H2 = nullptr;
  std::vector<float> *mH1H3 = nullptr;
  std::vector<float> *mH2H3 = nullptr;
  std::vector<float> *mHHH = nullptr;
  
  std::vector<int> *nJets = nullptr;

  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
};

#endif